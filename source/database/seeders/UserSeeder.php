<?php

namespace Database\Seeders;

use App\Services\Auth\AuthService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    private AuthService $service;

    public function __construct()
    {
        $this->service = resolve(AuthService::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $path = Config::get('filesystems.paths.users.database');
         $users = [];
         for ($i=0;$i<10;$i++) {
             $email = Str::random(10).'@amygdalab.com';
             $users[md5($email)] = $this->service->register($email,'password');
         }
         $content = json_encode($users);
         $fileName = 'users.json';
        if (!is_dir($path)) {
            mkdir($path,0777,true);
        }
        $filePath = "{$path}/{$fileName}";
        File::put($filePath,$content);
    }
}
