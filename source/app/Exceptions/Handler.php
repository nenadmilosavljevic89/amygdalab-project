<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

/*    public function render($request, Throwable $e)
    {
        $rendered = parent::render($request, $e);

        if (($e instanceof JSONErrorException) || (is_subclass_of($e,JSONErrorException::class))) {
            return $rendered;
        }
        return response()->json([
            'error' => [
                'code' => $rendered->getStatusCode(),
                'message' => $e->getMessage(),
            ]
        ], $rendered->getStatusCode());
    }*/
}
