<?php


namespace App\Exceptions;


use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class JSONErrorException extends \Exception
{
    private array $errors;
    public function __construct(array $errors,string $message,int $code = 400)
    {
        parent::__construct($message,$code);
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function render(): JsonResponse
    {
        return response()->json(['errors'=>$this->errors,'message'=>$this->message,'code'=>$this->code],$this->code);
    }

}
