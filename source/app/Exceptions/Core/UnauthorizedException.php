<?php

namespace App\Exceptions\Core;

use App\Exceptions\JSONErrorException;
use Illuminate\Http\Response;

class UnauthorizedException extends JSONErrorException
{

    public function __construct(string $message = null)
    {
        if (!$message) {
            $message = 'Unauthorized.';
        }
        parent::__construct([$message],$message,Response::HTTP_UNAUTHORIZED);
    }
}
