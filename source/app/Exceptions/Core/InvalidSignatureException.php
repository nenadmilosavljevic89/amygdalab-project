<?php

namespace App\Exceptions\Core;

use App\Exceptions\JSONErrorException;
use Illuminate\Http\Response;

class InvalidSignatureException extends JSONErrorException
{

    public function __construct()
    {
        parent::__construct(['The signature is invalid.'],'Invalid Signature.',Response::HTTP_FORBIDDEN);
    }
}
