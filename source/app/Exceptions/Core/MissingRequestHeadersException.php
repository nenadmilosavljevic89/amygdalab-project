<?php

namespace App\Exceptions\Core;

use App\Exceptions\JSONErrorException;
use Illuminate\Http\Response;

class MissingRequestHeadersException extends JSONErrorException
{

    public function __construct($errors)
    {
        parent::__construct($errors,'Missing Request Headers.',Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
