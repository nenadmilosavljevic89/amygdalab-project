<?php

namespace App\Exceptions\Auth;

use App\Exceptions\JSONErrorException;
use Illuminate\Http\Response;

class InvalidCredentialsException extends JSONErrorException
{

    public function __construct()
    {
        parent::__construct(['The provided credentials are incorrect.'],'Invalid Credentials.',Response::HTTP_UNAUTHORIZED);
    }
}
