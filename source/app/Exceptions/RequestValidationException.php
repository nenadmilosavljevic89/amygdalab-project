<?php


namespace App\Exceptions;


use Illuminate\Http\Response;

class RequestValidationException extends JSONErrorException
{
    protected $code = Response::HTTP_BAD_REQUEST;

    public function __construct(array $errors,string $message)
    {
        parent::__construct($errors,$message,$this->code);
    }

}
