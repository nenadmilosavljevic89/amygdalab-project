<?php


namespace App\Commands;



use App\Core\CQRS\Infrastructure\BaseCommand;

class TestCommand extends BaseCommand
{

    protected int $id;

    /**
     * TestCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        parent::__construct();
    }

    public function populate(int $id, string $test):void {
        $this->id = $id;
    }

    public function execute():void
    {
        dd("test command with id:{$this->id} executed");
        // TODO: Implement execute() method.
    }

    public function toArray(): array
    {
        return ['id'=>$this->id];
    }
}
