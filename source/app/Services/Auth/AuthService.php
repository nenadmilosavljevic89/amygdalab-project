<?php
namespace App\Services\Auth;

use App\Exceptions\Auth\InvalidCredentialsException;
use App\Models\Infrastructure\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthService
{
    private function getFilePath():string
    {
        $path = Config::get('filesystems.paths.users.database');
        $path = str_replace('storage/','',$path);

        $fileName = 'tokens.json';
        $fullPath = storage_path("{$path}/{$fileName}");
        return $fullPath;
    }

    public function register(string $email,string $password) {
         $hashed =  $this->hashPassword($password);
         //ToDo::implement check for unique email
        return new User($email,$hashed);
    }

    public function hashPassword($password): string
    {
        return Hash::make($password);
    }

    /**
     * @throws InvalidCredentialsException
     */
    public function checkPassword($plain, $hashed): void
    {
        if ($hashed && !Hash::check($plain, $hashed)) {
            throw new InvalidCredentialsException();
        }
    }

    public function generateToken(string $email)  {
        $token = Str::random(10);
        $path = $this->getFilePath();

        if (is_file($path)) {
            $content = json_decode(file_get_contents($path),true);
        }
        else {
            $content = [];
        }
        $content[md5($token)] =$email;
        File::put($path,json_encode($content));
        return $token;
    }
}
