<?php
namespace App\Services\UserManagement;

use App\Exceptions\Core\UnauthorizedException;
use App\Repositories\Domain\UserRepository;

class UserService
{
    private UserRepository $repository;

    public function __construct()
    {
        $this->repository = resolve(\App\Repositories\Infrastructure\UserRepository::class);
    }

    public function get(string $email) {
         $user = $this->repository->get($email);
         if (!$user) {
             throw new UnauthorizedException();
         }
         return $user;
    }
}
