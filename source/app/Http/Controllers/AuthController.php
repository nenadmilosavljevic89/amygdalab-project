<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Queries\Auth\LoginQuery;
use Illuminate\Http\JsonResponse;

class AuthController extends CQRSController
{
    public function login(LoginRequest $request) {
        $query = new LoginQuery($request->getEmail(),$request->getPassword());
        $token = $this->queryBus->handle($query);
        return new JsonResponse(['token' => $token]);

    }
}
