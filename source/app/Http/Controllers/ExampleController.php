<?php

namespace App\Http\Controllers;


use App\Commands\TestCommand;
use App\Request\TestRequest;


class ExampleController extends CQRSController
{

    public function index(TestRequest $request) {
        //
        $id = $request->getId();
        $testCommand = new TestCommand($id);
        $this->commandBus->dispatch($testCommand);
        return ['result'=>'created'];
    }

    //
}
