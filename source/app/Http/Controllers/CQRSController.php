<?php

namespace App\Http\Controllers;


use App\Core\CQRS\Infrastructure\CommandBus;
use App\Core\CQRS\Infrastructure\QueryBus;


class CQRSController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(protected CommandBus $commandBus, protected QueryBus $queryBus){

    }


}
