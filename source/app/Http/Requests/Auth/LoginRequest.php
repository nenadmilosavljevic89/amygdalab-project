<?php


namespace App\Http\Requests\Auth;


use App\Http\Requests\ParentRequest;

class LoginRequest extends ParentRequest
{
    protected ?string $email;
    protected ?string $password;

    public function rules(): array
    {

        return ['email' => 'required|string|email', 'password' => 'required|string'];
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }


    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }


}
