<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParentRequest extends FormRequest
{
    private array $requestData = [];

    public function validationData(): array
    {
        return $this->requestData;
    }

    /**
     */
    public function prepareForValidation(): void
    {
        $this->prepareValidationData();
    }

    public function validate(array $rules, ...$params): array
    {
        if ($this->autoValidateRequest()) {
            parent::validate($rules, $params);
        }

    }

    public function passedValidation():void
    {
        $this->populate();
    }

    protected function prepareValidationData(): void
    {
        $contentParams = $this->all();
        $routeParams = [];
        if ($this->route()) {
            $routeParams = $this->route()->parameters();
        }
        $this->requestData = array_replace_recursive($contentParams, $routeParams);
    }

    protected function populate(): void
    {
        foreach ($this->requestData as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    public function rules(): array
    {
        return [];
    }

    protected function autoValidateRequest(): bool
    {
        return true;
    }

    public function authorize(): bool
    {
        return true;
    }
}
