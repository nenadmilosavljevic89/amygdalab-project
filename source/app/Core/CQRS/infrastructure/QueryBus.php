<?php

namespace App\Core\CQRS\Infrastructure;

use App\Core\CQRS\Domain\Query;
use App\Core\CQRS\Domain\QueryBus as DomainQueryBus;
use ArtisanSdk\CQRS\Dispatcher;

class QueryBus implements DomainQueryBus
{
    /**
     * QueryBus constructor.
     */
    public function __construct(private Dispatcher $dispatcher)
    {
    }

    public function handle(Query $query):mixed
    {
        return $this->dispatcher->dispatch($query)->run();
    }
}
