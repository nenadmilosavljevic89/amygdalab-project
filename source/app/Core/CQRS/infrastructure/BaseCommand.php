<?php
namespace App\Core\CQRS\Infrastructure;

use ArtisanSdk\CQRS\Command;
use App\Core\CQRS\Domain\Command as DomainCommand;

abstract class BaseCommand extends Command implements DomainCommand
{
    use Argumentative;

    /**
     * BaseCommand constructor.
     */
    public function __construct()
    {
        $this->populateArguments();
    }

    public function toArray(): array
    {
        return [];
        // TODO: Implement toArray() method.
    }
}
