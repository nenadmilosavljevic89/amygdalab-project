<?php

namespace App\Core\CQRS\Infrastructure;

use App\Core\CQRS\Domain\Query as DomainQuery;
use \ArtisanSdk\CQRS\Query;

abstract class BaseQuery extends Query implements DomainQuery
{
    use Argumentative;
    public function __construct()
    {
        $this->populateArguments();
    }

    public function toArray(): array
    {
        return [];
    }
}
