<?php


namespace App\Core\CQRS\Infrastructure;


use ArtisanSdk\CQRS\Concerns\Arguments;

trait Argumentative
{
    use Arguments;

    private function populateArguments():void {
        $arguments = $this->toArray();
        $this->arguments($arguments);
    }
}
