<?php
namespace App\Core\CQRS\Infrastructure;

use App\Core\CQRS\Domain\Command;
use App\Core\CQRS\Domain\CommandBus as DomainCommandBus;
use ArtisanSdk\CQRS\Dispatcher;

class CommandBus implements DomainCommandBus
{


    /**
     * CommandBus constructor.
     */
    public function __construct(private Dispatcher $dispatcher)
    {
    }

    public function dispatch(Command $command):void
    {
        $this->dispatcher->dispatch($command)->run();
    }
}
