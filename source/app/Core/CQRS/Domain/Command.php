<?php
namespace App\Core\CQRS\Domain;

interface Command
{
    public function toArray(): array;
}
