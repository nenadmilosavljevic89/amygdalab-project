<?php
namespace App\Core\CQRS\Domain;

interface Query
{
    public function toArray(): array;
}
