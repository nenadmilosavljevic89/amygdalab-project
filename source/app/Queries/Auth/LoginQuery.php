<?php


namespace App\Queries\Auth;

use App\Core\CQRS\Infrastructure\BaseQuery;
use App\Exceptions\Core\UnauthorizedException;
use App\Exceptions\JSONErrorException;
use App\Services\Auth\AuthService;
use App\Services\UserManagement\UserService;

class LoginQuery extends BaseQuery
{

    protected string $email;
    protected string $password;
    private AuthService $authService;
    private UserService $userService;


    /**
     * @param string $email
     * @param string $password
     * @param string $deviceName
     */
    public function __construct(string $email, string $password)
    {

        $this->authService = resolve(AuthService::class);
        $this->userService = resolve(UserService::class);
        $this->email = $email;
        $this->password = $password;
        parent::__construct();
    }

    /**
     * @throws JSONErrorException
     * @throws \Symfony\Component\CssSelector\Exception\InternalErrorException
     */
    public function run(): mixed
    {
        $user = $this->userService->get($this->email);
        $this->authService->checkPassword($this->password,$user->password);
        return $this->authService->generateToken($this->email);
        /*$user = $this->service->getVerifiedByEmailOrFail($this->email);

        if (!$user->getPassword()) {
            throw new PasswordNotSetUpException();
        }

        $this->service->checkPassword($this->password,$user->getPassword());

        return $this->service->login($user, $this->deviceName);*/
    }

    public function toArray(): array
    {
        return ['email' => $this->email, 'password' => $this->password];
    }

    public function builder()
    {
    }
}
