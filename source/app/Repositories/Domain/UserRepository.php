<?php
namespace App\Repositories\Domain;

interface UserRepository
{
    public function get(string $email):?\App\Models\Domain\User;
}
