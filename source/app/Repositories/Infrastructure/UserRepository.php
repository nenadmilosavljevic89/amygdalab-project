<?php
namespace App\Repositories\Infrastructure;

use App\Models\Infrastructure\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class UserRepository implements \App\Repositories\Domain\UserRepository
{

    private function getFilePath():string
    {
        $path = Config::get('filesystems.paths.users.database');
        $path = str_replace('storage/','',$path);

        $fileName = 'users.json';
        $fullPath = storage_path("{$path}/{$fileName}");
        return $fullPath;
    }

    private function readUsers() {
        $path = $this->getFilePath();
        if (!is_file($path)) {
            throw new \Exception('database does not exist',500);
        }

        $content = file_get_contents($path);
        //ToDo:check if valid json
        return json_decode($content,true);
    }

    /**
     * @throws \Exception
     */
    public function get(string $email): ?\App\Models\Domain\User
    {
        $users = $this->readUsers();
        $index = md5($email);
        if (isset($users[$index])) {
            return new User($email,$users[$index]['password']);
        }
        return null;
    }
}
